from kafka import KafkaProducer, KafkaConsumer
import sys
import json
import time
import random
producer = KafkaProducer(bootstrap_servers='localhost:9092')
for i in range(1000):
   data={}
   data["idNews"]=str(random.randint(1,6))
   data["idUser"]=str(random.randint(1,4))
   tm=time.localtime(time.time()+random.randint(0,100000))
   
   data["time"]=str(random.randint(1,100000000))
   data["idType"]=str(random.randint(1,3))
   data = json.dumps(data).encode('utf-8')
   producer.send('sample', data)
   producer.flush()
sys.exit()

