import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class Test1 {
    String testString1 = "1, 2021-05-29 06:24:09.000000+0000, 1, 3";
    String testStringDict1 = "3, no";
    String testStringDict2 = "2, open and read";
    String testStringDict3 = "1, open and see";
    String testString3 = "1, 2021-05-29 06:24:09.000000+0000, 2, 3";
    String testString4 = "1, 2021-05-29 06:24:00.000000+0000, 2, 3";
    SparkSession ss = SparkSession
            .builder()
            .master("local")
            .appName("SparkSQLApplication")
            .getOrCreate();
    @Test
    public void test1() {

        JavaSparkContext sc = new JavaSparkContext(ss.sparkContext());
        JavaRDD<String> rdd1 = sc.parallelize(Arrays.asList(testString1));
        JavaRDD<String> rdd2 = sc.parallelize(Arrays.asList(testStringDict1, testStringDict2, testStringDict3));
        Query g = new Query();
        JavaRDD<String> res = g.returnCassandra(rdd1, rdd2);
        assert res.first().equals(" no, 1, 1");
   }

    @Test
    public void test2() {

        JavaSparkContext sc = new JavaSparkContext(ss.sparkContext());
        JavaRDD<String> rdd1 = sc.parallelize(Arrays.asList(testString1, testString1));

        JavaRDD<String> rdd2 = sc.parallelize(Arrays.asList(testStringDict1, testStringDict2, testStringDict3));
        Query g = new Query();
        JavaRDD<String> res = g.returnCassandra(rdd1, rdd2);
        assert res.toLocalIterator().next().equals(" no, 1, 1");
    }
    @Test
    public void test3() {

        JavaSparkContext sc = new JavaSparkContext(ss.sparkContext());
        JavaRDD<String> rdd1 = sc.parallelize(Arrays.asList(testString1, testString3));
        //CassandraJavaRDD<CassandraRow> rdd2 = sc.parallelize()
        JavaRDD<String> rdd2 = sc.parallelize(Arrays.asList(testStringDict1, testStringDict2, testStringDict3));
        Query g = new Query();
        JavaRDD<String> res = g.returnCassandra(rdd1, rdd2);
        List<String> l = res.take(2);
        assert l.get(0).equals(" no, 1, 1");
        assert l.get(1).equals(" no, 2, 1");
    }
    @Test
    public void test4() {

        JavaSparkContext sc = new JavaSparkContext(ss.sparkContext());
        JavaRDD<String> rdd1 = sc.parallelize(Arrays.asList(testString1, testString3, testString4));
        //CassandraJavaRDD<CassandraRow> rdd2 = sc.parallelize()
        JavaRDD<String> rdd2 = sc.parallelize(Arrays.asList(testStringDict1, testStringDict2, testStringDict3));
        Query g = new Query();
        JavaRDD<String> res = g.returnCassandra(rdd1, rdd2);
        List<String> l = res.take(2);
        assert l.get(0).equals(" no, 1, 1");
        assert l.get(1).equals(" no, 2, 2");
    }
}
