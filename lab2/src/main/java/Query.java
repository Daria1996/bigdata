import com.datastax.oss.driver.shaded.guava.common.collect.Lists;
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.CassandraRow;
import com.datastax.spark.connector.japi.SparkContextJavaFunctions;
import com.datastax.spark.connector.japi.rdd.CassandraJavaRDD;
import groovy.util.logging.Log4j;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Query: агригирует данные полученные из базыданных cassandra
 */
@Log4j
public class Query implements Serializable {
    private String regForDeleteSpace = "\\s+";
    private String regForInputString = ",";
    /**
     * В методе returnCassandra происходит агригирование данных.
     * @param rdd2 - данные полученные из таблицы cassandra
     *               iduser, tm, idtype, idnews
     *  @param rdd3 - словарь
     * @return агрегированные данные
     *
     */
    public JavaRDD<String> returnCassandra(JavaRDD<String> rdd2, JavaRDD<String> rdd3){

        rdd2 = rdd2.distinct();
        JavaPairRDD<Integer,String> newRdd2 = rdd2.mapToPair(new PairFunction<String, Integer, String>() {
            @Override
            public Tuple2<Integer, String> call(String s) throws Exception {
                String[] t = s.split(regForInputString);
                return new Tuple2<Integer, String>(Integer.parseInt(t[3].replaceAll(" ", "")), t[2].replaceAll(" ", ""));
            }
        });


        newRdd2.cache();
        JavaPairRDD<Integer,String> newRdd3 = rdd3.mapToPair(new PairFunction<String , Integer, String>() {
            @Override
            public Tuple2<Integer, String> call(String s) throws Exception {
                String[] t = s.split(regForInputString);
                return new Tuple2<Integer, String>(Integer.parseInt(t[0].replaceAll(regForDeleteSpace, "")), t[1]);
            }
        });

        newRdd3.cache();
        JavaPairRDD<Integer,Tuple2<String, String>> joinRdd = newRdd2.join(newRdd3);
        JavaPairRDD<Integer,String> joinRdd2 = joinRdd.mapToPair(new PairFunction<Tuple2<Integer, Tuple2<String, String>>, Integer, String>() {
            @Override
            public Tuple2<Integer, String> call(Tuple2<Integer, Tuple2<String, String>> integerTuple2Tuple2) throws Exception {

                return new Tuple2<Integer, String>(Integer.parseInt(integerTuple2Tuple2._2()._1()),integerTuple2Tuple2._2()._2());
            }
        });
        joinRdd2.cache();

        JavaPairRDD<String, Integer> rddPair1 = joinRdd2.groupBy(new Function<Tuple2<Integer, String>, String>() {
            @Override
            public String call(Tuple2<Integer, String> integerStringTuple2) throws Exception {
                return integerStringTuple2._2()+", "+integerStringTuple2._1();
            }
        })
                .mapToPair(new PairFunction<Tuple2<String, Iterable<Tuple2<Integer, String>>>, String, Integer>() {
                    @Override
                    public Tuple2<String, Integer> call(Tuple2<String, Iterable<Tuple2<Integer, String>>> stringIterableTuple2) throws Exception {
                        return new Tuple2<String,Integer>(stringIterableTuple2._1(), Lists.newArrayList(stringIterableTuple2._2()).size());
                    }
                });

        rddPair1.cache();
        JavaRDD<String> rddRes = rddPair1.map(new Function<Tuple2<String, Integer>, String>() {
            @Override
            public String call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                return stringIntegerTuple2._1()+", "+stringIntegerTuple2._2();
            }
        });
        rddRes.cache();
        return rddRes;

    }
}
