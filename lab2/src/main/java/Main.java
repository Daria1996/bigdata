
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import com.datastax.spark.connector.japi.CassandraRow;
import com.datastax.spark.connector.japi.rdd.CassandraJavaRDD;

import groovy.util.logging.Log4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import com.datastax.spark.connector.japi.SparkContextJavaFunctions;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * @author Daria Lashina
 * @version 1.0
 */
@Log4j
public class Main {
    private static String log4jConfPath = "/path/to/log4j.properties";

    /**
     *
     * @param args параметры командной строки
     *             args[0]-пространство ключей
     *             args[1]-метка времени
     *             args[2]-таблица с данными
     *             args[3]-таблица с обозначениями
     *             args[4]-столбец idNews
     *             args[5]-столбец idType
     *             args[6]-столбец id
     *             args[7]-столбец для обозначения типа взаимодействия
     *             args[8]-путь по которому будет сохранен каталог
     *             args[9]-столбец idUser
     */
    public static void main(String[] args) {
        String KeySpace = args[0];
        String tm = args[1];
        String infoTable = args[2];
        String dictTable = args[3];
        String idNews = args[4];
        String idType = args[5];;
        String id = args[6];
        String infer = args[7];
        String addr = args[8];
        String idUser = args[9];
        PropertyConfigurator.configure(log4jConfPath);
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkConf conf= new SparkConf(true)
                .set("spark.cassandra.connection.host","127.0.0.1").setAppName("Test1")
                .setMaster("local[4]");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkContextJavaFunctions functions = CassandraJavaUtil.javaFunctions(sc);
        CassandraJavaRDD<CassandraRow> rdd2 = functions.cassandraTable(KeySpace, infoTable);
        rdd2.cache();
        CassandraJavaRDD<CassandraRow> rdd3 = functions.cassandraTable(KeySpace, dictTable);
        rdd3.cache();
        JavaRDD<String> rddIn1 = rdd2.map(new Function<CassandraRow, String>() {
            @Override
            public String call(CassandraRow cassandraRow) throws Exception {
                return cassandraRow.getString(idUser)+", "+cassandraRow.getString(tm)
                        +", "+ cassandraRow.getString(idNews)+", "+cassandraRow.getString(idType);
            }
        });
        JavaRDD<String> rddIn2 = rdd3.map(new Function<CassandraRow, String>() {
            @Override
            public String call(CassandraRow cassandraRow) throws Exception {
                return cassandraRow.getString(id)+", "+cassandraRow.getString(infer);
            }
        });
        Query q = new Query();
        JavaRDD<String>rddPair1= q.returnCassandra(rddIn1, rddIn2);
        rddPair1.cache();
        rddPair1.saveAsTextFile(addr);
        sc.stop();
    }
}
