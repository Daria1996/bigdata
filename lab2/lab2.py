from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.query import SimpleStatement
import json
import sys
KEYSPACE=sys.argv[1]
cluster = Cluster(['127.0.0.1'])
session = cluster.connect()
session.execute("""
   CREATE KEYSPACE IF NOT EXISTS %s
   WITH replication = { 'class':'SimpleStrategy', 'replication_factor':'2'}
   """ % KEYSPACE)
session.set_keyspace(KEYSPACE)
session.execute("DROP TABLE IF EXISTS %s" % sys.argv[2] )
session.execute("DROP TABLE IF EXISTS %s" % sys.argv[3] )
session.execute("""
      CREATE TABLE IF NOT EXISTS %s(
         %s text,
         %s text,
         %s     text,
         %s text,
         PRIMARY KEY (%s, %s)
      )
      """ % (sys.argv[2],sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7], sys.argv[5], sys.argv[6]))
session.execute("""
   CREATE TABLE IF NOT EXISTS %s(
      %s text,
      %s text,
      PRIMARY KEY(%s)
   )
   """ % (sys.argv[3], sys.argv[8], sys.argv[9], sys.argv[8]))
session.execute("""INSERT INTO %s (%s, %s) VALUES('1', 'open and read')""" % (sys.argv[3], sys.argv[8], sys.argv[9]))
session.execute("""INSERT INTO %s (%s, %s) VALUES('2', 'open and see')""" % (sys.argv[3], sys.argv[8], sys.argv[9]))
session.execute("""INSERT INTO %s (%s, %s) VALUES('3', 'no')""" % (sys.argv[3],sys.argv[8], sys.argv[9]))
temp = """INSERT INTO """+ sys.argv[2] +""" ("""+sys.argv[4]+""", """+sys.argv[5]+""","""+sys.argv[7]+""", """+sys.argv[6]+""") VALUES(%s, %s, %s,%s)"""
from kafka import KafkaConsumer
consumer = KafkaConsumer('sample')
for message in consumer:
  entry = json.loads(message.value)
  session.execute(temp,(entry["idNews"], entry["idUser"], entry["idType"], entry["time"]))

  
