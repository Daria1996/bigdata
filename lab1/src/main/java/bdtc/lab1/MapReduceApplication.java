package bdtc.lab1;

import lombok.extern.log4j.Log4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import java.net.URI;

/**
 * @author Daria Lashina
 * @version 1.0
 */
@Log4j
public class MapReduceApplication extends Configured implements Tool {
    private static String matchNumber="\\d+";
    private static String uri = "hdfs://localhost:9000/user/root/";
    /**
     *
     * @param args параметры командной строки
     *             args[0]-входной файл
     *             args[1]-выходной файл
     *             args[2]-словарь обозначений
     *             args[3]-ширина агрегации
     *             args[4]-секунды(s), минуты(m), часы(h)
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
       int exitCode = ToolRunner.run(new MapReduceApplication(), args);


    }

    @Override
    public int run(String[] args) throws Exception {
        if (args.length < 5) {
            throw new RuntimeException("You should specify input and output folders!");
        }

        if (!args[4].equals("s") && !args[4].equals("m") && !args[4].equals("h")) {
            throw new RuntimeException("You should s or m or h!");
        }
        if(!args[3].matches(matchNumber)){
            throw new RuntimeException("You should number!");
        }

        uri+=args[2];
        Configuration conf = new Configuration();

        conf.set("mapreduce.output.sequencefileoutputformat.separator", ",");
        conf.set("smh",args[4]);
        conf.set("getParameters", args[3]);

        FileSystem fs = FileSystem.get(URI.create(uri), conf);

        FSDataInputStream in = fs.open(new Path(uri));

        String outStr = IOUtils.toString(in,"UTF-8");


        String[] ArrayData = outStr.split(" ");
        Integer count = ArrayData.length - 1;
        conf.set("countElements",Integer.toString(count));

        conf.set("timeCreate", ArrayData[0]);
        for (int i=0;i<count;i++){
            conf.set("dictElement"+i,ArrayData[i+1]);
        }

        Job job = Job.getInstance(conf, "Raw metrics");
        job.setJarByClass(MapReduceApplication.class);
        job.setMapperClass(HW1Mapper.class);
        job.setReducerClass(HW1Reducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        Path outputDirectory = new Path(args[1]);
        Path inputDirectory = new Path(args[0]);
        SequenceFileOutputFormat.setOutputPath(job, outputDirectory);

        FileInputFormat.addInputPath(job, inputDirectory);
        log.info("=====================JOB STARTED=====================");
        return job.waitForCompletion(true) ? 0 : 1;
        //log.info("=====================JOB ENDED=====================");
        // проверяем статистику по счётчикам
        // Counter counter = job.getCounters().findCounter(CounterType.MALFORMED);
        // log.info("=====================COUNTERS " + counter.getName() + ": " + counter.getValue() + "=====================");
    }
}
