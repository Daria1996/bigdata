package bdtc.lab1;
import lombok.extern.log4j.Log4j;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Редьюсер: суммирует все значения  полученные от {@link HW1Mapper}, выдаёт среднее значение агрегированной метрики
 */
@Log4j
public class HW1Reducer extends Reducer<Text, IntWritable, Text, Text> {
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        int count=0;

        while (values.iterator().hasNext()) {
            sum += values.iterator().next().get();
            count++;
        }

        context.write(key, new Text(Integer.toString(sum/count)));
    }
}
