package bdtc.lab1;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.HashMap;

public class HW1Mapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    /** Поле для хранения выходного значения*/
    private IntWritable value = new IntWritable();
    /** Поле для хранения ключа */
    private Text keyStr = new Text();
    /** Поле для хранения времени для агрегации в милисекунлах*/
    private Integer scaleMills;
    /** Поле для хранения времени для агрегации, который ввел пользователь*/
    private Integer scale;
    /** Поле для хранения времени для агрегации s-second,
     * m-minute,
     * h-hour*/
    private String smh;
    /** Поле для хранения первичной отметки времени*/
    private Long initialTime;
    /** Поле определяющее регулярное выражение для metricId*/
    private String matchIndex="\\d{1,3}";
    /** Поле определяющее регулярное выражение для timestamp*/
    private String matchTimeStamp="\\d{13}";
    /** Поле определяющее регулярное выражение для value*/
    private String matchValue = "\\d{1,5}";
    /** Поле для разделения id и idName*/
    private String regForDict = "-";

    private String regForDeleteSpace = "\\s+";
    private String regForInputString = ",";
    /** Поле хранящее словарь*/
    HashMap<String, String> dict = new HashMap<>();

    /**
     * В методе setup происходит преднастройка для map.
     * @param context-определяет контекст Job
     *                из него метод получает следующие данные:
     *                первичную метку времени, словарь, время агрегации
     */
    @Override
    protected void setup(Context context){
        Integer cnt = Integer.parseInt(context.getConfiguration().get("countElements"));
        for (int i=0; i<cnt; i++){

            String[] elDict = context.getConfiguration().get("dictElement"+i).split(regForDict);
           // elDict[1]=elDict[1].replace("\n","");
            dict.put(elDict[0], elDict[1]);
        }

        smh = context.getConfiguration().get("smh");
        scaleMills = Integer.parseInt(context.getConfiguration().get("getParameters"));
        scale = scaleMills;
        initialTime = Long.parseLong(context.getConfiguration().get("timeCreate"));
        switch (smh){
            case ("s"):
                scaleMills = scaleMills *1000;
                break;
            case("h"):
                scaleMills = scaleMills *3600000;
            case("m"):
                scaleMills = scaleMills *60000;
                break;
        }
    }

    /**
     * Функция для передачи преобразованного ключа функции reduce {@link HW1Reducer}
     * @param key
     * @param value
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String line = value.toString();
        String[] ArrayData = line.split(regForInputString);
        if (ArrayData.length == 3) {
            ArrayData[1] = ArrayData[1].replaceAll(regForDeleteSpace, "");
            ArrayData[2] = ArrayData[2].replaceAll(regForDeleteSpace, "");
            if (ArrayData[0].matches(matchIndex)
                    && ArrayData[1].matches(matchTimeStamp)
                    && ArrayData[2].matches(matchValue)) {
                String index = ArrayData[0];
                Long timeStamp = Long.parseLong(ArrayData[1]);

                Integer initialValue = Integer.parseInt(ArrayData[2]);
                keyStr.set("\n"+dict.get(index)+", "+ Long.toString(timeStamp - ((timeStamp - initialTime) % scaleMills))+", "+scale + smh+", ");
                context.write(keyStr, new IntWritable(initialValue));
            }
        }
    }
}
