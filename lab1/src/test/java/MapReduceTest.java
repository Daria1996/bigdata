
import bdtc.lab1.HW1Mapper;
import bdtc.lab1.HW1Reducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MapReduceTest {

    private MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
    private ReduceDriver<Text, IntWritable, Text, Text> reduceDriver;
    private MapReduceDriver<LongWritable, Text,Text, IntWritable, Text, Text> mapReduceDriver;

    private final String test1 = "3, 1616333435911, 616\n";
    private final String test2 = "1, 1616333441613, 10\n";
    private final String test3 = "3, 1616333440000, 100\n";
    private final String test4 = "3, 1616333435555, 500\n";
    private final String test5 = "2, 1616333441000, 100\n";
    private final String test6 = "4, 1616333451716, 9\n";
    @Before
    public void setUp() {
        HW1Mapper mapper = new HW1Mapper();
        HW1Reducer reducer = new HW1Reducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
        mapDriver.getContext().getConfiguration().set("smh","s");
        mapDriver.getContext().getConfiguration().set("getParameters","10");
        mapDriver.getContext().getConfiguration().set("timeCreate","1616333430866");
        mapReduceDriver.getConfiguration().set("smh","s");
        mapReduceDriver.getConfiguration().set("getParameters","10");
        mapReduceDriver.getConfiguration().set("timeCreate","1616333430866");

        mapReduceDriver.getConfiguration().set("dictElement0","1-Node1Cpu");
        mapReduceDriver.getConfiguration().set("dictElement1","2-Node2Cpu");
        mapReduceDriver.getConfiguration().set("dictElement2","3-Node1Rammb");
        mapReduceDriver.getConfiguration().set("dictElement3","4-Node2Rammb");
        mapReduceDriver.getConfiguration().set("countElements","4");
        mapDriver.getConfiguration().set("dictElement0","1-Node1Cpu");
        mapDriver.getConfiguration().set("dictElement1","2-Node2Cpu");
        mapDriver.getConfiguration().set("dictElement2","3-Node1Rammb");
        mapDriver.getConfiguration().set("dictElement3","4-Node2Rammb");
        mapDriver.getConfiguration().set("countElements","4");
    }

    /**
     * Тест проверяет работу {@link HW1Mapper}
     * @throws IOException
     */
    @Test
    public void testMapper() throws IOException {
        mapDriver
                .withInput(new LongWritable(), new Text(test1))
                .withInput(new LongWritable(), new Text(test2))
                .withInput(new LongWritable(), new Text("dxvx"))
                .withInput(new LongWritable(), new Text("dxvx, gfgd, fd"))
                .withInput(new LongWritable(), new Text("5, 5767, 5"))
                .withInput(new LongWritable(), new Text("5, 57673615789432, 5"))
                .withOutput(new Text("\n"+"Node1Rammb, "+"1616333430866, "+"10s, "), new IntWritable(616))
                .withOutput(new Text("\n"+"Node1Cpu, "+"1616333440866, "+ "10s, "), new IntWritable(10))
                .runTest();
    }
    /**
     * Тест проверяет работу {@link HW1Reducer}
     * @throws IOException
     */
    @Test
    public void testReducer() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();
        List<IntWritable> values2 = new ArrayList<IntWritable>();
        values.add(new IntWritable(616));
        values.add(new IntWritable(10));
        values2.add(new IntWritable(100));
        values2.add(new IntWritable(50));
        values2.add(new IntWritable(4));
        values2.add(new IntWritable(46));
        reduceDriver
                .withInput(new Text("\n"+"Node1Rammb, "+"1616333430866, "+ "10s, "), values)
                .withInput(new Text("\n"+"Node1Rammb, "+"1616333450866, "+"10s, "), values2)
                .withOutput(new Text("\n"+"Node1Rammb, "+"1616333430866, "+"10s, "), new Text("313"))
                .withOutput(new Text("\n"+"Node1Rammb, "+"1616333450866, "+ "10s, "), new Text("50"))
                .runTest();
    }
    /**
     * Тест проверяет работу {@link HW1Mapper} и {@link HW1Reducer}
     * @throws IOException
     */
    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver
                .withInput(new LongWritable(), new Text(test1))
                .withInput(new LongWritable(), new Text(test3))
                .withInput(new LongWritable(), new Text(test2))
                .withInput(new LongWritable(), new Text(test4))
                .withInput(new LongWritable(), new Text(test5))
                .withInput(new LongWritable(), new Text(test6))
                .withInput(new LongWritable(), new Text("dxvx"))
                .withInput(new LongWritable(), new Text("dxvx, gfgd, fd"))
                .withInput(new LongWritable(), new Text("5jk, 5767859642896, 5"))
                .withInput(new LongWritable(), new Text("5, 57673615789432, 5"))
                .withOutput(new Text("\n"+"Node1Cpu, "+"1616333440866, "+"10s, "), new Text("10"))
                .withOutput(new Text("\n"+"Node1Rammb, "+"1616333430866, "+"10s, "), new Text("405"))

                .withOutput(new Text("\n"+"Node2Cpu, "+"1616333440866, "+ "10s, "), new Text("100"))
                .withOutput(new Text("\n"+"Node2Rammb, "+"1616333450866, "+ "10s, "), new Text( "9"))
                .runTest();
    }


}
