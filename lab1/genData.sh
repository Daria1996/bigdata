#!/bin/bash
mod=2
if [[ $# -eq 0 ]] ; then
    echo 'You should specify output file!'
    exit 1
fi

if [[ $(($#%$mod)) -ne 0 ]] ; then
    echo 'odd parameters!'
    exit 1
fi

nowTime=$(date +%s%N)
nanoInMills=1000000
millsInSec=1000
nowMill=$(($nowTime/$nanoInMills))
inputDiff=$(($1*$millsInSec))
nowMill=$(($nowMill-$inputDiff))
rm -rf input
rm markTimeStamp
mkdir input
touch markTimeStamp
touch input/h.txt
dict="1-Node1Cpu 2-Node2Cpu 3-Node1Rammb 4-Node2Rammb"
echo "$nowMill $dict">>markTimeStamp
ArrayName=(1 2 3 4)
modInd=4
percent=100
modCpu=100
modRam=1000
difference=0
percent=100
percentBad=95
badString=("5" "4, 46435" "dgdg, gdsf" "dgdg, gdsf, 353" "434, 123, gfdg")
shift
while [ -n "$1" ] 
do
   for((i=0;i<$1;i++))
   do
      percentForBadString=$(($RANDOM%$percent))
      if [ $percentForBadString -gt $percentBad ]
      then
         number=$(($percentForBadString-$percentBad))
         echo "${badString[$number]}">>input/h.txt
      else
         number=$(($RANDOM%$modInd))
         now=$(date +%s%N)
         nowMill=$(($now/$nanoInMills))
         nowMill=$(($nowMill+$difference))
         val=0
         case $number in
            [0-1]) val=$(($RANDOM%$modCpu));;
            [2-3]) val=$(($RANDOM%$modRam));;
         esac
         echo "${ArrayName[$number]}, $nowMill, $val">>input/h.txt
      fi   
   done
   shift
   if [ -n "$1" ]
   then
      inputDiff=$(($1*$millsInSec))
      difference=$(($difference+$inputDiff))
   fi  
   shift
done
hdfs dfs -rm markTimeStamp
hdfs dfs -put markTimeStamp markTimeStamp
hdfs dfs -rm -r input
hdfs dfs -put input input
hdfs dfs -rm -r output
/opt/h*/bin/hadoop jar target/lab1-1.0-SNAPSHOT-jar-with-dependencies.jar input output markTimeStamp 10 s


